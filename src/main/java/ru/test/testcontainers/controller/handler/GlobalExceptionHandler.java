package ru.test.testcontainers.controller.handler;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseStatusException handleEmptyResultDataAccessException(EmptyResultDataAccessException ex) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found", ex);
    }

}
