package ru.test.testcontainers.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.test.testcontainers.dto.CreatePostDto;
import ru.test.testcontainers.dto.PostDto;
import ru.test.testcontainers.service.PostService;

import java.util.List;

@RequestMapping("/api/v1")
@RestController
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @PostMapping("/post/create/{user-id}")
    public ResponseEntity<PostDto> createPost(@PathVariable("user-id") Long userId,
                                              @RequestBody CreatePostDto postDto) {
        return new ResponseEntity<>(postService.create(postDto, userId), HttpStatus.CREATED);
    }

    @GetMapping("/post/all/{user-id}")
    public ResponseEntity<List<PostDto>> getPostsByUser(@PathVariable("user-id") Long userId) {
        return ResponseEntity.ok(postService.getByUser(userId));
    }

}
