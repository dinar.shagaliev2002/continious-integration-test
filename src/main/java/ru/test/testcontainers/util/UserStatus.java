package ru.test.testcontainers.util;

public enum UserStatus {

    ACTIVE,
    BANNED,
    DELETED

}
