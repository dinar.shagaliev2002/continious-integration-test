package ru.test.testcontainers.dto;

import lombok.Builder;
import lombok.Data;
import ru.test.testcontainers.entity.User;

@Data
@Builder
public class UserDto {

    private Long id;

    private String email;

    private String firstname;

    private String lastname;

    private String patronymic;

    public static UserDto from(User user) {
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .patronymic(user.getPatronymic())
                .build();
    }

}
