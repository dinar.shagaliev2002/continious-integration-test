package ru.test.testcontainers.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreatePostDto {

    private String text;

}
