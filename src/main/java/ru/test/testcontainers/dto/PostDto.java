package ru.test.testcontainers.dto;

import lombok.Builder;
import lombok.Data;
import ru.test.testcontainers.entity.Post;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class PostDto {

    private Long id;

    private String text;

    private Long userId;

    public static PostDto from(Post post) {
        return PostDto.builder()
                .id(post.getId())
                .text(post.getText())
                .userId(post.getUser().getId())
                .build();
    }

    public static List<PostDto> from(List<Post> postList) {
        return postList.stream().map(PostDto::from).collect(Collectors.toList());
    }

}
