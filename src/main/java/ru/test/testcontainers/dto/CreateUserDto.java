package ru.test.testcontainers.dto;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class CreateUserDto {

    @NotBlank(message = "email is mandatory")
    private String email;

    @Length(min = 8, max = 16)
    @NotBlank(message = "password is mandatory")
    private String password;

    private String firstname;

    private String lastname;

}
