package ru.test.testcontainers.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.test.testcontainers.dto.CreateUserDto;
import ru.test.testcontainers.dto.UserDto;
import ru.test.testcontainers.entity.User;
import ru.test.testcontainers.repository.UserJpaRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserJpaRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserDto create(CreateUserDto userDto) {
        User user = User.builder()
                .email(userDto.getEmail())
                .firstname(userDto.getFirstname())
                .lastname(userDto.getLastname())
                .postList(new ArrayList<>())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .build();

        log.info("User: " + user.toString() + " successfully created");
        return UserDto.from(userRepository.save(user));
    }

    public UserDto getUser(Long userId) {
        return UserDto.from(findUser(userId));
    }

    public User findUser(Long userId) {
        return  userRepository.findById(userId).orElseThrow(
                () -> {
                    String errorMsg = "User with id {" + userId + "} doesn't exist";
                    log.error(errorMsg);
                    return new EmptyResultDataAccessException(errorMsg, 1);
                });
    }

    @Transactional
    public void delete(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> {
                    String errorMsg = "User with id {" + id + "} doesn't exist";
                    log.error(errorMsg);
                    return new EmptyResultDataAccessException(errorMsg, 1);
                });

        userRepository.delete(user);

        log.info("User with id {} successfully deleted", id);
    }

}
