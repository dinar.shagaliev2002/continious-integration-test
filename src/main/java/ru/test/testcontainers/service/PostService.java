package ru.test.testcontainers.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.test.testcontainers.dto.CreatePostDto;
import ru.test.testcontainers.dto.PostDto;
import ru.test.testcontainers.entity.Post;
import ru.test.testcontainers.entity.User;
import ru.test.testcontainers.repository.PostJpaRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostService {

    @Autowired
    private final PostJpaRepository postRepository;

    private final UserService userService;


    public PostDto create(CreatePostDto postDto,
                          Long userId) {
        User user = userService.findUser(userId);

        Post post = Post.builder()
                .text(postDto.getText())
                .user(user)
                .build();

        return PostDto.from(postRepository.save(post));
    }

    public List<PostDto> getByUser(Long userId) {
        User user = userService.findUser(userId);
        return PostDto.from(postRepository.findAllByUser(user));
    }

}
