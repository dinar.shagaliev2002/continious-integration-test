package ru.test.testcontainers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.test.testcontainers.entity.User;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
}
