package ru.test.testcontainers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.test.testcontainers.entity.Post;
import ru.test.testcontainers.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostJpaRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByUser(User user);

}
