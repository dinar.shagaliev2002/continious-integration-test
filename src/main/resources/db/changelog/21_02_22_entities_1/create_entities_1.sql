--liquibase formatted sql

--changeset Dinar Shagaliev:2.1

/*
 create table user_account
*/
CREATE TABLE user_account (
  id BIGSERIAL NOT NULL,
  firstname VARCHAR(255),
  lastname VARCHAR(255),
  patronymic VARCHAR(255),
  email VARCHAR(255),
  password VARCHAR(255),
  status VARCHAR(255),
  CONSTRAINT pk_user_account PRIMARY KEY (id)
);

/*
 create table post
*/
CREATE TABLE post (
  id BIGSERIAL NOT NULL,
  text VARCHAR(5000),
  user_id BIGINT,
  CONSTRAINT pk_post PRIMARY KEY (id)
);

ALTER TABLE post ADD CONSTRAINT FK_POST_ON_USER FOREIGN KEY (user_id) REFERENCES user_account (id);

