package ru.test.testcontainers.util;

public class TestApi {

    public static final String API = "/api/v1";

    public static final String USER_CREATE_URL = API + "/user/";
    public static final String USER_GET_URL = API + "/user/{id}";
    public static final String USER_DELETE_URL = API + "/user/{id}";

    public static final String POST_CREATE_URL = API + "/post/create";
    public static final String POST_GET_URL = API + "/post/all/{id}";

}
