package ru.test.testcontainers.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import ru.test.testcontainers.dto.CreateUserDto;
import ru.test.testcontainers.dto.UserDto;
import ru.test.testcontainers.testcontainer.TestPostgresBase;
import ru.test.testcontainers.util.TestApi;

class TestUserController extends TestPostgresBase {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final CreateUserDto createUserDto = CreateUserDto.builder()
            .email("somebody@email.com")
            .firstname("somefirstname")
            .lastname("somelastname")
            .password("somepassword")
            .build();

    /* unnecessary check

	@Disabled
	@BeforeEach
	@DisplayName("assert that postgres is running")
	void assertDatabase() {
		assertTrue(TestPostgresBase.getContainer().isRunning());
	}
     */

    // WebClient просмотреть/переписать
    // unix sockets
    // PLSQL
    // Boni Garcia, Mastering Software Testing
    // Gitlab CI (GitLab CI configuration)
    // Sonarqube

    // cisco connect
    // rdp

    @Nested
    class GetRequest {

        @Test
        @Rollback
        @DisplayName("positive test - get user")
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = {"classpath:scripts/truncateUserAccount.sql",
                        "classpath:scripts/insertDinar.sql"})
        void testGetSingleUser() {
            long id = 1;

            UserDto user = UserDto.builder()
                    .id(id)
                    .firstname("Dinar")
                    .lastname("Shagaliev")
                    .email("dinar.shagaliev@gmail.com")
                    .build();

            webTestClient.get().uri(TestApi.USER_GET_URL, id)
                    .exchange()
                    .expectStatus().isOk()
                    .expectBody(UserDto.class)
                    .isEqualTo(user);
        }

    }

    @Nested
    class CreateRequest {

        @Test
        @Rollback
        @DisplayName("positive test - create new user")
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = {"classpath:scripts/truncateUserAccount.sql"})
        void testCreateUser() {
            UserDto userDto = UserDto.builder()
                    .id(1L)
                    .firstname(createUserDto.getFirstname())
                    .lastname(createUserDto.getLastname())
                    .email(createUserDto.getEmail())
                    .build();

            webTestClient.post().uri(TestApi.USER_CREATE_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.just(createUserDto), CreateUserDto.class)
                    .exchange()
                    .expectStatus().isOk()
                    .expectBody(UserDto.class)
                    .isEqualTo(userDto);

        }

        @Test
        @DisplayName("negative test - create new user with null pass")
        void testCreateUserNullPassword() {
            createUserDto.setPassword(null);

            webTestClient.post().uri(TestApi.USER_CREATE_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Mono.just(createUserDto), CreateUserDto.class)
                    .exchange()
                    .expectStatus().isBadRequest();
        }

        @Test
        @DisplayName("negative test - create new user with blank pass")
        void testCreateUserBlankPassword() {
            createUserDto.setPassword("");

            webTestClient.post().uri(TestApi.USER_CREATE_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Mono.just(createUserDto), CreateUserDto.class)
                    .exchange()
                    .expectStatus().isBadRequest();
        }

    }

    @Nested
    class DeleteRequest {

        @Test
        @Rollback
        @DisplayName("positive test - delete user")
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = {"classpath:scripts/truncateUserAccount.sql",
                        "classpath:scripts/insertDinar.sql"})
        void testDeleteUser() {
            long id = 1;

            webTestClient.delete().uri(TestApi.USER_DELETE_URL, id)
                    .exchange()
                    .expectStatus().isOk();
        }

        @Test
        @DisplayName("negative test - delete not exist user")
        void testDeleteNotExistUser() {
            long id = 1;

            webTestClient.delete().uri(TestApi.USER_DELETE_URL, id)
                    .exchange()
                    .expectStatus()
                    .isNotFound();
        }

    }

}