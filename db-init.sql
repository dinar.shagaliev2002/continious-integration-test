-- POSTGRES INITIAL SCRIPT

create database test_container;
create user test_container with encrypted password 'test_container';
grant all privileges on database test_container to test_container;